/******************************************************************************
 *
 * Name: acsavos.h - OS specific defines, etc. for SavOS
 *
 *****************************************************************************/

#ifndef __ACSAVOS_H__
#define __ACSAVOS_H__

#define ACPI_NO_ERROR_MESSAGES 0
#define ACPI_DEBUG_OUTPUT 1

/* Host-dependent types and defines for in-kernel ACPICA */

#define ACPI_MACHINE_WIDTH          32

#define ACPI_REDUCED_HARDWARE 1
#define ACPI_USE_LOCAL_CACHE 1
#define ACPI_USE_NATIVE_DIVIDE

#define ACPI_USE_SYSTEM_CLIBRARY 1

#ifndef __cdecl
#define __cdecl
#endif

#endif /* __ACSAVOS_H__ */
